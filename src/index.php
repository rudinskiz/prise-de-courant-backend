<?php

use PriseDeCourant\Core\Kernel;

// Define constants.
define('WEB_ROOT', __DIR__);
// Load Composer classes.
require_once '../vendor/autoload.php';
// Run the app kernel.
Kernel::run();
