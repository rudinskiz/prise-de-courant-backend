<?php

namespace PriseDeCourant\Controller;

use PriseDeCourant\Core\Exception\AccessException;
use PriseDeCourant\Core\Exception\ParameterNotFoundException;
use PriseDeCourant\Model\Reservation;
use PriseDeCourant\Model\Table;
use PriseDeCourant\Services\ServiceContainer;
use PriseDeCourant\Services\SmsService;
use PriseDeCourant\Services\ReservationService;
use PriseDeCourant\Services\TokenService;
use PriseDeCourant\Utils\Response;
use PriseDeCourant\Utils\ResponseCode;
use PriseDeCourant\Validators\UserValidator;

class ReservationController extends TableController
{

    protected static SmsService $smsService;

    public static function initializeController()
    {
        parent::initializeController();
        self::$smsService = ServiceContainer::get('sms');
        self::$reservationService = ServiceContainer::get('reservation');
    }

    public static function getByCode()
    {
        try {
            // Get the parameters.
            $reservationCode = self::$requestService->getQueryWithException('code');
            // Get the reservation.
            /** @var Reservation $reservation */
            $reservation = self::$reservationRepository->findById($reservationCode);
            if ($reservation === NULL) {
                throw new ParameterNotFoundException('reservation');
            }
            Response::printArray(array_merge($reservation->toArray(), ['items' => self::$reservationService->getReservationItems($reservation)]));
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        }
    }

    public static function cancelReservation()
    {
        try {
            // Get the parameters.
            $reservationCode = self::$requestService->getQueryWithException('code');
            // Get the reservation.
            /** @var Reservation $reservation */
            $reservation = self::$reservationRepository->findById($reservationCode);
            if ($reservation === NULL || $reservation->isCancelled()) {
                throw new ParameterNotFoundException('reservation');
            }
            // Send an SMS to the user.
            self::$smsService->send($reservation->getUser()->getInternationalNumber(), "Your reservation ($reservationCode) has been cancelled.");
            // Update
            self::$reservationRepository->update(['reservationId' => $reservation->id()], ['state' => 'cancelled']);
            Response::printMessage('Cancelled.');
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        }
    }

    public static function addDiscount()
    {
        try {
            // Get the parameters.
            $reservationCode = self::$requestService->getQueryWithException('code');
            // Get the reservation.
            /** @var Reservation $reservation */
            $reservation = self::$reservationRepository->findById($reservationCode);
            if ($reservation === NULL || $reservation->isCancelled()) {
                throw new ParameterNotFoundException('reservation');
            }
            // Send an SMS to the user.
            self::$smsService->send($reservation->getUser()->getInternationalNumber(), "Your reservation ($reservationCode) has been approved for a discount.");
            // Update
            self::$reservationRepository->update(['reservationId' => $reservation->id()], ['discount' => TRUE]);
            Response::printMessage('Applied discount.');
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        }
    }

    public static function reserveTable()
    {
        try {
            $token = self::$tokenService->getToken($_ENV['TOKEN_TYPE_ACCESS_TOKEN'], self::$requestService->getParameterWithException('token'));
            // Get user.
            $user = $token->getUser();
            if (!UserValidator::isRole($user, $_ENV['DEFAULT_ROLE']) && !UserValidator::isRole($user, $_ENV['ADMIN_ROLE'])) {
                throw new AccessException('You cannot create reservations.');
            }
            // Get parameters.
            /** @var Table $table */
            $table = self::$tableRepository->findById(self::$requestService->getParameterWithException('table'));
            $reservationStart = self::$requestService->getParameterWithException('start');
            $reservationEnd = self::$requestService->getParameterWithException('end');
            // Try to parse the dates.
            $reservationStart = \DateTime::createFromFormat('Y-m-d H:i:s', $reservationStart);
            $reservationEnd = \DateTime::createFromFormat('Y-m-d H:i:s', $reservationEnd);
            if ($reservationStart === FALSE || $reservationEnd === FALSE) {
                throw new ParameterNotFoundException('date');
            }
            if ($reservationStart > $reservationEnd) {
                [$reservationEnd, $reservationStart] = [$reservationStart, $reservationEnd];
            }
            if ($table === NULL) {
                throw new ParameterNotFoundException('table');
            }
            // Check availability.
            if (!self::$reservationService->checkAvailability($table->id(), $reservationStart, $reservationEnd)) {
                throw new AccessException('Table is not available.');
            }
            // Create a reservation code.
            $reservationCode = self::$tokenService->generator->generateString($_ENV['TOKEN_SIZE'], TokenService::characterList);
            // Create a reservation.
            self::$reservationRepository->insert([
                'startDate' => $reservationStart->format('Y-m-d H:i:s'),
                'endDate' => $reservationEnd->format('Y-m-d H:i:s'),
                'reservationCode' => $reservationCode,
                'tableId' => $table->id(),
                'userId' => $user->id(),
                'comment' => '',
            ]);
            // Send an SMS message to the user with the reservation code.
            self::$smsService->send($user->getInternationalNumber(), "Thank your for your reservation! Your reservation code is: $reservationCode");

            Response::printArray([
                'reservationCode' => $reservationCode,
            ]);
        } catch (AccessException $accessException) {
            Response::printError($accessException, ResponseCode::UNAUTHORIZED);
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        }
    }

    public static function setComment()
    {
        try {
            $token = self::$tokenService->getToken($_ENV['TOKEN_TYPE_ACCESS_TOKEN'], self::$requestService->getParameterWithException('token'));
            // Get user.
            $user = $token->getUser();
            if (!UserValidator::isRole($user, $_ENV['WORKER_ROLE']) && !UserValidator::isRole($user, $_ENV['ADMIN_ROLE'])) {
                throw new AccessException('You cannot set comments.');
            }
            $comment = self::$requestService->getParameterWithException('comment', '');
            // Get the reservation.
            /** @var Reservation $reservation */
            $reservation = self::$reservationRepository->findById(self::$requestService->getParameterWithException('reservationId'));
            if ($reservation === NULL || $reservation->isCancelled()) {
                throw new ParameterNotFoundException('reservation');
            }
            // Set the comment.
            self::$reservationRepository->update(['reservationId' => $reservation->id()], ['comment' => $comment]);
            Response::printMessage('Comment set.');
        } catch (AccessException $accessException) {
            Response::printError($accessException, ResponseCode::BAD_REQUEST);
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        }
    }

    public static function getReservations()
    {
        try {
            $token = self::$tokenService->getToken($_ENV['TOKEN_TYPE_ACCESS_TOKEN'], self::$requestService->getQueryWithException('token'));
            // Get user.
            $user = $token->getUser();
            if (!UserValidator::isRole($user, $_ENV['DEFAULT_ROLE']) && !UserValidator::isRole($user, $_ENV['ADMIN_ROLE'])) {
                throw new AccessException('You cannot get reservations in a bulk.');
            }
            /** @var Reservation[] $reservations */
            $reservations = [];
            if (UserValidator::isRole($user, $_ENV['DEFAULT_ROLE'])) {
                /** @var Reservation[] $reservations */
                $reservations = self::$reservationRepository->findWhere(['userId' => $user->id()]);
            } else {
                /** @var Reservation[] $reservations */
                $reservations = self::$reservationRepository->findAll();
            }
            $reservations = array_filter($reservations, function ($reservation) {
                return !$reservation->isCancelled();
            });
            // Map the reservations.
            $reservations = array_map(function ($r) {
                return $r->toArray();
            }, $reservations);

            Response::printArray($reservations);
        } catch (AccessException $accessException) {
            Response::printError($accessException, ResponseCode::BAD_REQUEST);
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        }
    }
}
