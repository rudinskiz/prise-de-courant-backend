<?php

namespace PriseDeCourant\Controller;

use PriseDeCourant\Core\Exception\AccessException;
use PriseDeCourant\Core\Exception\DatabaseException;
use PriseDeCourant\Core\Exception\ParameterNotFoundException;
use PriseDeCourant\Core\Exception\ResponseException;
use PriseDeCourant\Core\Exception\ValidationException;
use PriseDeCourant\Model\AccessToken;
use PriseDeCourant\Model\Repository\AccessTokenRepository;
use PriseDeCourant\Model\Repository\RoleRepository;
use PriseDeCourant\Model\Repository\UserRepository;
use PriseDeCourant\Model\Role;
use PriseDeCourant\Model\User;
use PriseDeCourant\Services\ServiceContainer;
use PriseDeCourant\Services\TokenService;
use PriseDeCourant\Utils\Response;
use PriseDeCourant\Utils\ResponseCode;
use PriseDeCourant\Validators\FieldValidator;
use PriseDeCourant\Validators\UserValidator;

/**
 * A controller that handles all the user logic.
 *
 * @package PriseDeCourant\Controller
 */
class UserController extends BaseController
{

    private static RoleRepository $roleRepository;
    private static UserRepository $userRepository;
    private static AccessTokenRepository $accessTokenRepository;
    private static TokenService $tokenService;

    /**
     * {@inheritDoc}
     */
    public static function initializeController()
    {
        parent::initializeController();
        self::$roleRepository = new RoleRepository();
        self::$userRepository = new UserRepository();
        self::$accessTokenRepository = new AccessTokenRepository();
        self::$tokenService = ServiceContainer::get('token');
    }

    /**
     * Handle user registration logic.
     */
    public static function register()
    {
        // Create a user.
        try {
            // Get the parameters.
            $firstName = self::$requestService->getParameterWithException('firstName');
            $lastName = self::$requestService->getParameterWithException('lastName');
            $email = self::$requestService->getParameterWithException('email');
            $password = self::$requestService->getParameterWithException('password');
            $phoneNumber = self::$requestService->getParameterWithException('phoneNumber');
            // Validate the parameters.
            if (!FieldValidator::validateAll([
                FieldValidator::FIELD_NOT_EMPTY => [$firstName, $lastName],
                FieldValidator::FIELD_EMAIL => [$email],
                FieldValidator::FIELD_PASSWORD => [$password],
                FieldValidator::FIELD_PHONE_NUMBER => [$phoneNumber]
            ])) {
                throw new ValidationException('user');
            }
            // Get the default role.
            /** @var Role $role */
            $role = self::$roleRepository->getByName($_ENV['DEFAULT_ROLE']);
            if (empty($role)) {
                throw new ParameterNotFoundException('role');
            }
            // Check if there is an user with a specified e-mail.
            if (self::$userRepository->getByEmail($email) !== NULL) {
                throw new DatabaseException('E-mail already taken.');
            }
            // Save to the database.
            $user = self::$userRepository->insert([
                'firstName' => $firstName,
                'lastName' => $lastName,
                'password' => password_hash($password, PASSWORD_DEFAULT),
                'email' => $email,
                'phoneNumber' => $phoneNumber,
                'roleId' => $role->id(),
                'verified' => 0,
            ]);
            // Create a verification token.
            $token = ServiceContainer::get('token')->generate($user, $_ENV['TOKEN_TYPE_EMAIL_VERIFICATION']);
            // Send a verification e-mail.
            ServiceContainer::get('mail')->send($email, 'Prise de Courant Account Verification', ServiceContainer::get('template')->render('email/verification.twig', [
                'user' => $user,
                'token' => $token,
                'verificationUrl' => sprintf('%s/verification?token=%s', $_ENV['FRONTEND_URL'], $token->getToken()),
            ]));

            Response::printMessage('Registration successful');
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        } catch (DatabaseException $databaseException) {
            Response::printError($databaseException, ResponseCode::BAD_REQUEST);
        } catch (ValidationException $validationException) {
            Response::printError($validationException, ResponseCode::BAD_REQUEST);
        }
    }

    /**
     * Handle user login logic.
     */
    public static function login()
    {
        // Try to create an access token and return it.
        try {
            // Get the parameters.
            $email = self::$requestService->getParameterWithException('email');
            $password = self::$requestService->getParameterWithException('password');
            // Get the user via e-mail.
            /** @var User $user */
            $user = self::$userRepository->getByEmail($email);
            if ($user === NULL) {
                throw new AccessException('No valid users found.');
            }
            // Try to login.
            if (!$user->isVerified()) {
                throw new AccessException('User not verified.');
            }
            if (!$user->isPasswordCorrect($password)) {
                throw new AccessException('Invalid password.');
            }
            // Create a new token.
            /** @var AccessToken $token */
            $token = ServiceContainer::get('token')->generate($user, $_ENV['TOKEN_TYPE_ACCESS_TOKEN']);
            Response::printArray([
                'token' => $token->label(),
                'expirationDate' => $token->getExpirationDate()->format('Y-m-d H:i:s'),
            ]);
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        } catch (AccessException $accessException) {
            Response::printError($accessException, ResponseCode::BAD_REQUEST);
        } catch (\PDOException $pdoException) {
            Response::printError(new DatabaseException($pdoException->getMessage()), ResponseCode::SERVER_ERROR);
        }
    }

    /**
     * Handle account verification logic.
     */
    public static function verifyAccount()
    {
        // Try to verify the user via token.
        try {
            // Get the token.
            /** @var AccessToken $token */
            $token = ServiceContainer::get('token')->getToken($_ENV['TOKEN_TYPE_EMAIL_VERIFICATION'], self::$requestService->getQueryWithException('token'));
            // Get user.
            /** @var User $user */
            $user = $token->getUser();
            if ($user === NULL) {
                throw new ParameterNotFoundException('user');
            }
            if ($user->isVerified()) {
                throw new AccessException('User is already verified.');
            }
            // Update the user.
            self::$userRepository->update(['userId' => $user->id()], [
                'verified' => TRUE,
            ]);
            // Delete the old token.
            self::$accessTokenRepository->delete(['token' => $token->getToken()]);

            Response::printMessage('User verified');
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        } catch (AccessException $accessException) {
            Response::printError($accessException, ResponseCode::SERVER_ERROR);
        }
    }

    /**
     * Get user information.
     */
    public static function getInformation()
    {
        // Try to verify the user via token.
        try {
            // Get token.
            /** @var AccessToken $token */
            $token = ServiceContainer::get('token')->getToken($_ENV['TOKEN_TYPE_ACCESS_TOKEN'], self::$requestService->getQueryWithException('token'));
            // Get the user information.
            /** @var User $user */
            $user = $token->getUser();
            if ($user === NULL) {
                throw new ParameterNotFoundException('user');
            }
            if (!$user->isVerified()) {
                throw new AccessException('User is not verified.');
            }

            Response::printArray([
                'name' => $user->label(),
                'email' => $user->getEmail(),
                'phoneNumber' => $user->getNumber(),
                'role' => $user->getRole(),
            ]);
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        } catch (AccessException $accessException) {
            Response::printError($accessException, ResponseCode::SERVER_ERROR);
        }
    }

    /**
     * Handle password reset request.
     */
    public static function requestPasswordReset()
    {
        // Send a password reset request.
        try {
            // Get the email.
            $email = self::$requestService->getQueryWithException('email');
            // Get the user.
            /** @var User $user */
            $user = self::$userRepository->getByEmail($email);
            if ($user === NULL) {
                throw new ParameterNotFoundException('user');
            }
            if (!$user->isVerified()) {
                throw new AccessException('User is not verified.');
            }
            // Create a password reset token.
            $token = ServiceContainer::get('token')->generate($user, $_ENV['TOKEN_TYPE_RESET_PASSWORD']);
            // Send out a reset password email.
            ServiceContainer::get('mail')->send($email, 'Prise de Courant Password Reset', ServiceContainer::get('template')->render('email/passwordReset.twig', [
                'user' => $user,
                'token' => $token,
                'resetPasswordUrl' => sprintf('%s/resetPassword?token=%s', $_ENV['FRONTEND_URL'], $token->getToken()),
            ]));
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        } catch (AccessException $accessException) {
            Response::printError($accessException, ResponseCode::BAD_REQUEST);
        }
    }

    /**
     * Handle password reset logic.
     */
    public static function resetPassword()
    {
        // Reset the password.
        try {
            // Get the new password.
            $password = self::$requestService->getParameterWithException('password');
            $tokenValue = self::$requestService->getParameterWithException('token');
            // Validate.
            if (!FieldValidator::validateField($password, FieldValidator::FIELD_PASSWORD)) {
                throw new ValidationException('password');
            }
            // Get token.
            /** @var AccessToken $token */
            $token = ServiceContainer::get('token')->getToken($_ENV['TOKEN_TYPE_RESET_PASSWORD'], $tokenValue);
            // Get user.
            /** @var User $user */
            $user = $token->getUser();
            if ($user === NULL) {
                throw new ParameterNotFoundException('user');
            }
            if (!$user->isVerified()) {
                throw new AccessException('User is not verified.');
            }
            // Update the user.
            self::$userRepository->update(['userId' => $user->id()], [
                'password' => password_hash($password, PASSWORD_DEFAULT),
            ]);
            // Delete the old token.
            self::$accessTokenRepository->delete(['token' => $tokenValue]);
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        } catch (AccessException $accessException) {
            Response::printError($accessException, ResponseCode::BAD_REQUEST);
        } catch (ValidationException $validationException) {
            Response::printError($validationException, ResponseCode::BAD_REQUEST);
        }
    }

    /**
     * Handle password reset logic.
     */
    public static function resendVerificationMail()
    {
        // Create a user.
        try {
            // Get the parameters.
            $email = self::$requestService->getQueryWithException('email');
            // Check if there is an user with a specified e-mail.
            /** @var User $user */
            $user = self::$userRepository->getByEmail($email);
            if ($user === NULL) {
                throw new ParameterNotFoundException('user');
            }
            // Check if the user is already verified.
            if ($user->isVerified()) {
                throw new AccessException('User is already verified.');
            }
            // Create a verification token.
            $token = ServiceContainer::get('token')->generate($user, $_ENV['TOKEN_TYPE_EMAIL_VERIFICATION']);
            // Send a verification e-mail.
            ServiceContainer::get('mail')->send($email, 'Prise de Courant Account Verification', ServiceContainer::get('template')->render('email/verification.twig', [
                'user' => $user,
                'token' => $token,
                'verificationUrl' => sprintf('%s/verification?token=%s', $_ENV['FRONTEND_URL'], $token->getToken()),
            ]));

            Response::printMessage('Verification resent.');
        } catch (AccessException $accessException) {
            Response::printError($accessException, ResponseCode::BAD_REQUEST);
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        } catch (DatabaseException $databaseException) {
            Response::printError($databaseException, ResponseCode::BAD_REQUEST);
        }
    }

    /**
     * Handle user fields update logic.
     */
    public static function updateUserInformation()
    {
        try {
            // Get the parameters.
            $tokenValue = self::$requestService->getParameterWithException('token');
            $fields = (array) (self::$requestService->getParameterWithException('values'));
            /** @var AccessToken $token */
            $token = ServiceContainer::get('token')->getToken($_ENV['TOKEN_TYPE_ACCESS_TOKEN'], $tokenValue);
            // Get the user.
            $user = $token->getUser();
            if ($user === NULL) {
                throw new ParameterNotFoundException('user');
            }
            if (!$user->isVerified()) {
                throw new AccessException('User is not verified.');
            }
            // If changing the name be sure to split it properly.
            if (isset($fields['name'])) {
                $fullName = explode(' ', $fields['name']);
                $fields['lastName'] = array_pop($fullName);
                $fields['firstName'] = implode(' ', $fullName);
                unset($fields['name']);
            }
            // Verify the fields.
            foreach ($fields as $fieldName => $field) {
                if (!property_exists($user, $fieldName)) {
                    throw new AccessException("Invalid property $fieldName.");
                }
                if (!FieldValidator::validateField($field, FieldValidator::FIELD_NOT_EMPTY)) {
                    throw new ValidationException($fieldName);
                }
            }
            // Handle special keys && validations.
            if (isset($fields['userId'])) {
                throw new AccessException('You cannot change your user ID.');
            }
            if (isset($fields['phoneNumber'])) {
                if (!FieldValidator::validateField($fields['phoneNumber'], FieldValidator::FIELD_PHONE_NUMBER)) {
                    throw new ValidationException('phoneNumber');
                }
            }
            if (isset($fields['email'])) {
                if (!FieldValidator::validateField($fields['email'], FieldValidator::FIELD_EMAIL)) {
                    throw new ValidationException('email');
                }
                // TODO: E-mail change should warrant re-verification?
            }
            if (isset($fields['password'])) {
                if (!FieldValidator::validateField($fields['password'], FieldValidator::FIELD_PASSWORD)) {
                    throw new ValidationException('password');
                }
                $fields['password'] = password_hash($fields['password'], PASSWORD_DEFAULT);
            }
            // Update the user.
            self::$userRepository->update(['userId' => $user->id()], $fields);

            Response::printMessage('Updated user.');
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        } catch (AccessException $accessException) {
            Response::printError($accessException, ResponseCode::BAD_REQUEST);
        } catch (ValidationException $validationException) {
            Response::printError($validationException, ResponseCode::BAD_REQUEST);
        }
    }

    public static function masquerade()
    {
        try {
            // Get token.
            $token = self::$tokenService->getToken($_ENV['TOKEN_TYPE_ACCESS_TOKEN'], self::$requestService->getParameterWithException('token'));
            // Get the user information.
            /** @var User $user */
            $user = $token->getUser();
            if (!UserValidator::isRole($user, $_ENV['ADMIN_ROLE'])) {
                throw new AccessException('Only administrators can masquerade.');
            }
            // Get the other user information.
            $email = self::$requestService->getParameterWithException('email');
            // Get the user via e-mail.
            /** @var User $user */
            $user = self::$userRepository->getByEmail($email);
            if ($user === NULL) {
                throw new AccessException('No valid users found.');
            }
            // Try to login.
            if (!$user->isVerified()) {
                throw new AccessException('User not verified.');
            }
            // Create a new token.
            $token = self::$tokenService->generate($user, $_ENV['TOKEN_TYPE_ACCESS_TOKEN']);

            Response::printArray([
                'token' => $token->label(),
                'expirationDate' => $token->getExpirationDate()->format('Y-m-d H:i:s'),
            ]);
        } catch (AccessException $accessException) {
            Response::printError($accessException, ResponseCode::UNAUTHORIZED);
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        }
    }
}
