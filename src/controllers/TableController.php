<?php

namespace PriseDeCourant\Controller;

use PriseDeCourant\Core\Exception\AccessException;
use PriseDeCourant\Core\Exception\DatabaseException;
use PriseDeCourant\Core\Exception\ParameterNotFoundException;
use PriseDeCourant\Core\Exception\ValidationException;
use PriseDeCourant\Model\Repository\ReservationRepository;
use PriseDeCourant\Model\Repository\TableRepository;
use PriseDeCourant\Model\Table;
use PriseDeCourant\Services\ReservationService;
use PriseDeCourant\Services\ServiceContainer;
use PriseDeCourant\Services\TokenService;
use PriseDeCourant\Utils\Response;
use PriseDeCourant\Utils\ResponseCode;
use PriseDeCourant\Validators\FieldValidator;
use PriseDeCourant\Validators\UserValidator;

class TableController extends BaseController
{

    protected static ReservationRepository $reservationRepository;
    protected static TableRepository $tableRepository;
    protected static TokenService $tokenService;
    protected static ReservationService $reservationService;

    /**
     * {@inheritDoc}
     */
    public static function initializeController()
    {
        parent::initializeController();
        self::$tableRepository = new TableRepository();
        self::$reservationRepository = new ReservationRepository();
        self::$tokenService = ServiceContainer::get('token');
        self::$reservationService = ServiceContainer::get('reservation');
    }

    /**
     * Handles the creation of new tables.
     */
    public static function createTable()
    {
        // Create a table.
        try {
            // Get the parameters.
            $tableName = self::$requestService->getParameterWithException('tableName');
            $tableDescription = self::$requestService->getParameterWithException('tableDescription');
            $numberOfSeats = (int) self::$requestService->getParameterWithException('numberOfSeats');
            $smokingAllowed = (int) self::$requestService->getParameterWithException('smokingAllowed');
            // Validate parameters.
            if (!FieldValidator::validateAll([
                FieldValidator::FIELD_NOT_EMPTY => [$tableName, $tableDescription],
                FieldValidator::FIELD_NUMBER_RANGE => [[$numberOfSeats, [FieldValidator::FIELD_RANGE_MIN => 1]]]
            ])) {
                throw new ValidationException('table');
            }
            // Validate user.
            $token = self::$tokenService->getToken($_ENV['TOKEN_TYPE_ACCESS_TOKEN'], self::$requestService->getParameterWithException('token'));
            if (!UserValidator::isRole($token->getUser(), $_ENV['ADMIN_ROLE'])) {
                throw new AccessException('You are not an administrator.');
            }
            // Create the table.
            self::$tableRepository->insert([
                'tableName' => $tableName,
                'tableDescription' => $tableDescription,
                'numberOfSeats' => $numberOfSeats,
                'smokingAllowed' => $smokingAllowed,
            ]);

            Response::printMessage('Table created.');
        } catch (ValidationException $validationException) {
            Response::printError($validationException, ResponseCode::BAD_REQUEST);
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        } catch (AccessException $accessException) {
            Response::printError($accessException, ResponseCode::BAD_REQUEST);
        } catch (\PDOException $PDOException) {
            Response::printError(new DatabaseException($PDOException->getMessage()), ResponseCode::SERVER_ERROR);
        }
    }

    /**
     * Handle table removal.
     */
    public static function removeTable()
    {
        // Remove the table.
        try {
            // Get the parameters.
            $tableId = (int) self::$requestService->getQueryWithException('tableId');
            $token = self::$tokenService->getToken($_ENV['TOKEN_TYPE_ACCESS_TOKEN'], self::$requestService->getQueryWithException('token'));
            // Validate user.
            if (!UserValidator::isRole($token->getUser(), $_ENV['ADMIN_ROLE'])) {
                throw new AccessException('You are not an administrator.');
            }
            // Delete the table.
            if (self::$tableRepository->findById($tableId)) {
                self::$tableRepository->delete(['tableId' => $tableId]);
                Response::printMessage('Table removed.');
            } else {
                throw new ParameterNotFoundException('table');
            }
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        } catch (AccessException $accessException) {
            Response::printError($accessException, ResponseCode::BAD_REQUEST);
        }
    }

    /**
     * Handle table information update.
     */
    public static function updateTableInformation()
    {
        // Create a table.
        try {
            // Get the parameters.
            $fields = (array) (self::$requestService->getParameterWithException('values'));
            /** @var Table $table */
            $table = self::$tableRepository->findById(self::$requestService->getParameterWithException('tableId'));
            $token = self::$tokenService->getToken($_ENV['TOKEN_TYPE_ACCESS_TOKEN'], self::$requestService->getParameterWithException('token'));
            // Validate user.
            if (!UserValidator::isRole($token->getUser(), $_ENV['ADMIN_ROLE'])) {
                throw new AccessException('You are not an administrator.');
            }
            // Validate the fields.
            foreach ($fields as $fieldName => $field) {
                if (!property_exists($table, $fieldName)) {
                    throw new AccessException("Invalid property $fieldName.");
                }
                if (!FieldValidator::validateField($field, FieldValidator::FIELD_NOT_EMPTY) && $fieldName !== 'smokingAllowed') {
                    throw new ValidationException($fieldName);
                }
            }
            // Handle special keys & validations.
            if (isset($fields['tableId'])) {
                throw new AccessException('You cannot change the table ID.');
            }
            if (isset($fields['numberOfSeats'])) {
                if (!FieldValidator::validateField($fields['numberOfSeats'], FieldValidator::FIELD_NUMBER_RANGE, [
                    FieldValidator::FIELD_RANGE_MIN => 1
                ])) {
                    throw new ValidationException('numberOfSeats');
                }
            }
            if (isset($fields['smokingAllowed'])) {
                $fields['smokingAllowed'] = (int) $fields['smokingAllowed'];
            }
            // Update the table.
            self::$tableRepository->update(['tableId' => $table->id()], $fields);

            Response::printMessage('Table updated.');
        } catch (ValidationException $validationException) {
            Response::printError($validationException, ResponseCode::BAD_REQUEST);
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        } catch (AccessException $accessException) {
            Response::printError($accessException, ResponseCode::BAD_REQUEST);
        } catch (\PDOException $PDOException) {
            Response::printError(new DatabaseException($PDOException->getMessage()), ResponseCode::SERVER_ERROR);
        }
    }

    /**
     * Get a specific table by it's ID.
     */
    public static function getTableInformation()
    {
        try {
            /** @var Table $table */
            $table = self::$tableRepository->findById(self::$requestService->getQueryWithException('tableId'));
            if ($table === NULL) {
                throw new ParameterNotFoundException('table');
            }

            Response::printArray($table->toArray());
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        }
    }

    /**
     * Get multiple tables.
     */
    public static function getTables()
    {
        $skip = (int) self::$requestService->getQuery('skip', 0);
        $limit = (int) self::$requestService->getQuery('limit', 10);
        $filterName = self::$requestService->getQuery('filter', '');
        // Check if we have a filter.
        if (mb_strlen($filterName) > 0) {
            /** @var Table[] $tables */
            $tables = array_slice(self::$tableRepository->findWhere([
                'tableName' => [
                    'operator' => 'LIKE',
                    'value' => "%$filterName%"
                ],
                [
                    'tableId',
                    'tableName',
                    'tableDescription',
                    'numberOfSeats',
                    'smokingAllowed'
                ],
            ]), $skip, $limit);
            // Get total count with condition.
            $count = self::$tableRepository->count([
                'tableName' => [
                    'operator' => 'LIKE',
                    'value' => "%$filterName%"
                ],
            ]);
        } else {
            // Get all tables.
            /** @var Table[] $tables */
            $tables = self::$tableRepository->findAll([
                'tableId',
                'tableName',
                'tableDescription',
                'numberOfSeats',
                'smokingAllowed'
            ], $limit, $skip);
            // Get count of all.
            $count = self::$tableRepository->count();
        }

        Response::printArray([
            'count' => $count,
            'data' => array_map(function ($table) {
                return $table->toArray();
            }, $tables)
        ]);
    }

    /**
     * Check table availability.
     */
    public static function checkAvailability()
    {
        try {
            $tableId = (int) self::$requestService->getQueryWithException('table');
            $reservationStart = self::$requestService->getQueryWithException('start');
            $reservationEnd = self::$requestService->getQueryWithException('end');
            $token = self::$tokenService->getToken($_ENV['TOKEN_TYPE_ACCESS_TOKEN'], self::$requestService->getQueryWithException('token'));
            // Get user.
            $user = $token->getUser();
            if (!UserValidator::isRole($user, $_ENV['DEFAULT_ROLE']) && !UserValidator::isRole($user, $_ENV['ADMIN_ROLE'])) {
                throw new AccessException('You cannot view table availability.');
            }
            // Try to parse the dates.
            $reservationStart = \DateTime::createFromFormat('Y-m-d H:i:s', $reservationStart);
            $reservationEnd = \DateTime::createFromFormat('Y-m-d H:i:s', $reservationEnd);
            if ($reservationStart === FALSE || $reservationEnd === FALSE) {
                throw new ParameterNotFoundException('date');
            }
            if ($reservationStart > $reservationEnd) {
                [$reservationEnd, $reservationStart] = [$reservationStart, $reservationEnd];
            }
            // Filter out the reservations.
            Response::printArray([
                'available' => self::$reservationService->checkAvailability($tableId, $reservationStart, $reservationEnd),
            ]);
        } catch (AccessException $accessException) {
            Response::printError($accessException, ResponseCode::UNAUTHORIZED);
        } catch (ParameterNotFoundException $parameterNotFoundException) {
            Response::printError($parameterNotFoundException, ResponseCode::BAD_REQUEST);
        }
    }
}
