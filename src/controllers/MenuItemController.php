<?php

namespace PriseDeCourant\Controller;

use PriseDeCourant\Core\Exception\DatabaseException;
use PriseDeCourant\Core\Exception\ParameterNotFoundException;
use PriseDeCourant\Model\Reservation;
use PriseDeCourant\Model\Repository\ReservationRepository;
use PriseDeCourant\Model\Repository\MenuItemRepository;
use PriseDeCourant\Services\ServiceContainer;
use PriseDeCourant\Services\TokenService;
use PriseDeCourant\Services\ReservationService;
use PriseDeCourant\Utils\Response;
use PriseDeCourant\Utils\ResponseCode;

class MenuItemController extends BaseController
{
	protected static MenuItemRepository $menuItemRepository;
	protected static ReservationRepository $reservationRepository;
  protected static TokenService $tokenService;
  protected static ReservationService $reservationService;

	/**
	 * {@inheritdoc}
	 */
	public static function initializeController()	
	{
		parent::initializeController();
		self::$menuItemRepository = new MenuItemRepository();
		self::$reservationRepository = new ReservationRepository();
    self::$tokenService = ServiceContainer::get('token');
    self::$reservationService = ServiceContainer::get('reservation');
	}

	/**
	 * Get a list of all menu items.
	 */
	public static function getMenuItems()
	{
		try {
		    $menuItems = array_map(function ($item) {
				return $item->toArray();
			}, self::$menuItemRepository->findAll());

			Response::printArray($menuItems);
		} catch (\PDOException $PDOException) {
		    Response::printError(new DatabaseException($PDOException->getMessage()), ResponseCode::SERVER_ERROR);
		}
	}

	/**
	 * Adds a new menu item to the bill.
	 */
	public static function addToOrder() 
	{
    try {
      // Get token.
      $token = self::$tokenService->getToken($_ENV['TOKEN_TYPE_ACCESS_TOKEN'], self::$requestService->getQueryWithException('token'));
      // Get the parameters.
      $reservationCode = self::$requestService->getQueryWithException('code');
      $menuItemId = self::$requestService->getQueryWithException('menuItem');
      // Get the reservation.
      /** @var Reservation $reservation */
      $reservation = self::$reservationRepository->findById($reservationCode); 

      if ($reservation === NULL || $reservation->isCancelled() || $reservation->getUser()->id() !== $token->getUser()->id()) {
        throw new ParameterNotFoundException('reservation');
      }
      self::$reservationService->addItemToReservation($reservation, $menuItemId);
      Response::printMessage('Menu item added to bill.');
		} catch (\PDOException $PDOException) {
		  Response::printError(new DatabaseException($PDOException->getMessage()), ResponseCode::SERVER_ERROR);
		}
  }

	/**
	 * Removes a menu item from the bill.
	 */
	public static function removeFromOrder() 
	{
    try {
      // Get token.
      $token = self::$tokenService->getToken($_ENV['TOKEN_TYPE_ACCESS_TOKEN'], self::$requestService->getQueryWithException('token'));
      // Get the parameters.
      $reservationCode = self::$requestService->getQueryWithException('code');
      $menuItemId = self::$requestService->getQueryWithException('menuItem');
      // Get the reservation.
      /** @var Reservation $reservation */
      $reservation = self::$reservationRepository->findById($reservationCode); 

      if ($reservation === NULL || $reservation->isCancelled() || $reservation->getUser()->id() !== $token->getUser()->id()) {
        throw new ParameterNotFoundException('reservation');
      }
      self::$reservationService->removeItemFromReservation($reservation, $menuItemId);
      Response::printMessage('Menu item removed from the bill.');
		} catch (\PDOException $PDOException) {
		  Response::printError(new DatabaseException($PDOException->getMessage()), ResponseCode::SERVER_ERROR);
		}
  }

  /**
   * Finalizes the reservation.
   */
  public static function finalizeReservation()
  {
    try { 
      // Get token.
      $token = self::$tokenService->getToken($_ENV['TOKEN_TYPE_ACCESS_TOKEN'], self::$requestService->getQueryWithException('token'));
      // Get the parameters.
      $reservationCode = self::$requestService->getQueryWithException('code');
      // Get the reservation.
      /** @var Reservation $reservation */
      $reservation = self::$reservationRepository->findById($reservationCode); 

      if ($reservation === NULL || $reservation->isCancelled() || $reservation->getUser()->id() !== $token->getUser()->id()) {
        throw new ParameterNotFoundException('reservation');
      }

      self::$reservationService->setOrderStatus($reservation, 'finished');
      Response::printMessage('Reservation finalized.');
    }
    catch (\PDOException $PDOException) {
      Response::printError(new DatabaseException($PDOException->getMessage()), ResponseCode::SERVER_ERROR);
    }
  }
}
