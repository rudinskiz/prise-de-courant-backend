<?php

namespace PriseDeCourant\Controller;

use PriseDeCourant\Core\Exception\NotFoundException;

class ErrorController extends BaseController
{

    /**
     * If a resource wasn't found.
     *
     * @throws NotFoundException
     */
    public static function notFound()
    {
        throw new NotFoundException();
    }
}
