<?php

namespace PriseDeCourant\Controller;

use PriseDeCourant\Utils\Response;

/**
 * A controller that makes sure that the connection to the API is good.
 *
 * @package PriseDeCourant\Controller
 */
class PingController extends BaseController
{

    /**
     * Return some basic information about the current API version.
     */
    public static function ping()
    {
        $pingMessage = sprintf('%s %s', $_ENV['API_NAME'], $_ENV['API_VERSION']);
        Response::printMessage($pingMessage);
    }
}
