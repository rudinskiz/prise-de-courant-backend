<?php

namespace PriseDeCourant\Controller;

use PriseDeCourant\Services\RequestService;
use PriseDeCourant\Services\ServiceContainer;

/**
 * Base controller that all other controllers inherit from.
 *
 * @package PriseDeCourant\Controller
 */
abstract class BaseController
{

    protected static ?RequestService $requestService = NULL;

    /**
     *  Initializes base controllers.
     */
    public static function initializeController()
    {
        if (self::$requestService === NULL) {
            self::$requestService = ServiceContainer::get('request');
        }
    }
}
