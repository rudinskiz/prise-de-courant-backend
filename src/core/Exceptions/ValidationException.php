<?php

namespace PriseDeCourant\Core\Exception;

use JetBrains\PhpStorm\Pure;
use PriseDeCourant\Utils\ResponseCode;

/**
 * Exception thrown when a specified parameter is not valid.
 *
 * @package PriseDeCourant\Core\Exception
 */
class ValidationException extends ResponseException
{

    /**
     * {@inheritDoc}
     */
    #[Pure] public function __construct($parameterName, $code = ResponseCode::BAD_REQUEST)
    {
        parent::__construct(sprintf('Specified parameter: "%s" was is not valid.', $parameterName), $code);
    }
}
