<?php

namespace PriseDeCourant\Core\Exception;

use JetBrains\PhpStorm\Pure;
use PriseDeCourant\Utils\ResponseCode;

/**
 * Exception thrown when a specified parameter is not found.
 *
 * @package PriseDeCourant\Core\Exception
 */
class ParameterNotFoundException extends ResponseException
{

    /**
     * {@inheritDoc}
     */
    #[Pure] public function __construct($parameterName, $code = ResponseCode::SERVER_ERROR)
    {
        parent::__construct(sprintf('Specified parameter: "%s" was not found.', $parameterName), $code);
    }
}
