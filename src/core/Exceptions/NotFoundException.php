<?php

namespace PriseDeCourant\Core\Exception;

/**
 * Exception thrown when a specified resource is not found.
 *
 * @package PriseDeCourant\Core\Exception
 */
class NotFoundException extends ResponseException
{
}
