<?php

namespace PriseDeCourant\Core\Exception;

/**
 * Exception thrown when access is not given.
 *
 * @package PriseDeCourant\Core\Exception
 */
class AccessException extends ResponseException
{
}
