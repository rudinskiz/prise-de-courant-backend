<?php

namespace PriseDeCourant\Core\Exception;

/**
 * Exception thrown when a database error occurs.
 *
 * @package PriseDeCourant\Core\Exception
 */
class DatabaseException extends ResponseException
{
}
