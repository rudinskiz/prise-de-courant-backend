<?php

namespace PriseDeCourant\Core\Exception;

use JetBrains\PhpStorm\Pure;
use PriseDeCourant\Utils\ResponseCode;

/**
 * Base Exception class for HTTP responses.
 *
 * @package PriseDeCourant\Core\Exception
 */
abstract class ResponseException extends \Exception
{

    /**
     * ResponseException constructor.
     *
     * @param string $message
     *   Exception message.
     * @param int $code
     *   HTTP status code.
     */
    #[Pure] public function __construct($message = "", $code = ResponseCode::SERVER_ERROR)
    {
        parent::__construct($message, $code);
    }
}
