<?php

namespace PriseDeCourant\Core;

use Dotenv\Dotenv;
use PriseDeCourant\Controller\BaseController;
use PriseDeCourant\Controller\ErrorController;
use PriseDeCourant\Controller\PingController;
use PriseDeCourant\Controller\ReservationController;
use PriseDeCourant\Controller\TableController;
use PriseDeCourant\Controller\MenuItemController;
use PriseDeCourant\Controller\UserController;
use PriseDeCourant\Core\Exception\NotFoundException;
use PriseDeCourant\Utils\Response;
use PriseDeCourant\Utils\ResponseCode;
use Steampixel\Route;

/**
 * Kernel of the application.
 * Sets up the routes as well as loads some necessary components.
 *
 * @package PriseDeCourant\Core
 */
class Kernel
{

    /**
     * Load Environment files and set the appropriate header.
     */
    public static function configure()
    {
        // Load the .env
        Dotenv::createImmutable(WEB_ROOT . '/../')->load();
        // Set the header.
        header('Content-Type: application/json');
        // Disable CORS.
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: *");
    }

    /**
     * Setup the router.
     */
    public static function setupRoutes()
    {
        // Initialize controllers.
        ErrorController::initializeController();
        UserController::initializeController();
        TableController::initializeController();
        ReservationController::initializeController();
        MenuItemController::initializeController();
        // Configure router.
        Route::pathNotFound([ErrorController::class, 'notFound']);
        // Ping Controller routes.
        Route::add('/ping', [PingController::class, 'ping']);
        // User Controller routes.
        Route::add('/user/register', [UserController::class, 'register'], 'POST');
        Route::add('/user/login', [UserController::class, 'login'], 'POST');
        Route::add('/user/verifyAccount', [UserController::class, 'verifyAccount'], 'GET');
        Route::add('/user/getInformation', [UserController::class, 'getInformation'], 'GET');
        Route::add('/user/resetPassword', [UserController::class, 'requestPasswordReset'], 'GET');
        Route::add('/user/resetPassword', [UserController::class, 'resetPassword'], 'POST');
        Route::add('/user/resendVerification', [UserController::class, 'resendVerificationMail'], 'GET');
        Route::add('/user/updateInformation', [UserController::class, 'updateUserInformation'], 'POST');
        Route::add('/user/masquerade', [UserController::class, 'masquerade'], 'POST');
        // Table Controller routes.
        Route::add('/table/createTable', [TableController::class, 'createTable'], 'POST');
        Route::add('/table/deleteTable', [TableController::class, 'removeTable'], 'GET');
        Route::add('/table/updateTableInformation', [TableController::class, 'updateTableInformation'], 'POST');
        Route::add('/table/getTableInformation', [TableController::class, 'getTableInformation'], 'GET');
        Route::add('/table/getTablesInformation', [TableController::class, 'getTables'], 'GET');
        Route::add('/table/checkAvailability', [TableController::class, 'checkAvailability'], 'GET');
        // Reservation Controller routes.
        Route::add('/reservation/getByCode', [ReservationController::class, 'getByCode'], 'GET');
        Route::add('/reservation/cancel', [ReservationController::class, 'cancelReservation'], 'POST');
        Route::add('/reservation/create', [ReservationController::class, 'reserveTable'], 'POST');
        Route::add('/reservation/comment', [ReservationController::class, 'setComment'], 'POST');
        Route::add('/reservations', [ReservationController::class, 'getReservations'], 'GET');
        Route::add('/reservation/applyDiscount', [ReservationController::class, 'addDiscount'], 'POST');
        // Menu Item Controller routes.
        Route::add('/menu/list', [MenuItemController::class, 'getMenuItems']);
        Route::add('/menu/addItem', [MenuItemController::class, 'addToOrder']);
        Route::add('/menu/removeItem', [MenuItemController::class, 'removeFromOrder']);
        Route::add('/reservation/finalize', [MenuItemController::class, 'finalizeReservation']);
    }

    /**
     * Handle the request.
     */
    public static function run()
    {
        self::configure();
        self::setupRoutes();

        try {
            Route::run('/api');
        } catch (NotFoundException $notFoundException) {
            Response::printError($notFoundException, ResponseCode::NOT_FOUND);
        }
    }
}
