<?php

namespace PriseDeCourant\Model;

use PriseDeCourant\Model\Repository\TableRepository;
use PriseDeCourant\Model\Repository\UserRepository;

class Reservation extends BaseModel
{

    protected int $reservationId;
    protected string $startDate;
    protected string $endDate;
    protected string $reservationCode;
    protected int $userId;
    protected int $tableId;
    protected string $comment;
    protected bool $discount;
    protected string $state;

    /**
     * {@inheritdoc}
     */
    public function id(): int
    {
        return $this->reservationId;
    }

    /**
     * {@inheritdoc}
     */
    public function label(): string
    {
        return $this->reservationCode;
    }

    /**
     * Check if the reservation is cancelled.
     *
     * @return bool
     *   TRUE if cancelled.
     */
    public function isCancelled(): bool
    {
        return $this->state === 'cancelled';
    }

    /**
     * Get reservation range.
     *
     * @return array
     *   Reservation range.
     */
    public function getRange(): array
    {
        return [
            'start' => \DateTime::createFromFormat('Y-m-d H:i:s', $this->startDate),
            'end' => \DateTime::createFromFormat('Y-m-d H:i:s', $this->endDate),
        ];
    }

    /**
     * Return a reserved table.
     *
     * @return Table
     *   Reserved table.
     */
    public function getTable(): Table
    {
        return (new TableRepository())->findById($this->tableId);
    }

    /**
     * Return a user who reserved the table.
     *
     * @return User
     *   Reserved table.
     */
    public function getUser(): User
    {
        return (new UserRepository())->findById($this->userId);
    }

    /**
     * {@inheritDoc}
     */
    public function toArray(): array
    {
        return array_merge($this->getRange(), [
            'reservationCode' => $this->reservationCode,
            'user' => $this->getUser()->toArray(),
            'table' => $this->getTable()->toArray(),
            'comment' => $this->comment,
            'discount' => $this->discount,
            'state' => $this->state,
        ]);
    }
}
