<?php

namespace PriseDeCourant\Model;

class TokenType extends BaseModel
{

    protected int $tokenTypeId;
    protected string $tokenType;

    /**
     * {@inheritdoc}
     */
    public function id(): int
    {
        return $this->tokenTypeId;
    }

    /**
     * {@inheritdoc}
     */
    public function label(): string
    {
        return $this->tokenType;
    }
}
