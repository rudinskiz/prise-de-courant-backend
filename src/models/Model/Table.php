<?php

namespace PriseDeCourant\Model;

use JetBrains\PhpStorm\ArrayShape;

class Table extends BaseModel
{

    protected int $tableId;
    protected string $tableName;
    protected string $tableDescription;
    protected int $numberOfSeats;
    public bool $smokingAllowed;

    /**
     * {@inheritdoc}
     */
    public function id(): int
    {
        return $this->tableId;
    }

    /**
     * {@inheritdoc}
     */
    public function label(): string
    {
        return $this->tableName;
    }

    /**
     * Get table description.
     *
     * @return string
     *   Table description.
     */
    public function getDescription(): string
    {
        return $this->tableDescription;
    }

    /**
     * Get number of seats.
     *
     * @return int
     *   Number of seats.
     */
    public function getNumberOfSeats(): int
    {
        return $this->numberOfSeats;
    }

    /**
     * Check if smoking is allowed at the table.
     *
     * @return bool
     *   True if smoking is allowed.
     */
    public function isSmokingAllowed(): bool
    {
        return $this->smokingAllowed;
    }

    /**
     * {@inheritDoc}
     */
    public function toArray(): array
    {
        return [
            'tableId' => $this->tableId,
            'tableName' => $this->tableName,
            'tableDescription' => $this->tableDescription,
            'numberOfSeats' => $this->numberOfSeats,
            'smokingAllowed' => $this->smokingAllowed,
        ];
    }
}
