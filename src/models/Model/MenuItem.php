<?php

namespace PriseDeCourant\Model;

class MenuItem extends BaseModel
{

    protected int $menuItemId;
    protected string $name;
    protected float $price;

    /**
     * {@inheritdoc}
     */
    public function id(): int
    {
        return $this->menuItemId;
    }

    /**
     * {@inheritdoc}
     */
    public function label(): string
    {
			return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        return [
            'id' => $this->menuItemId,
            'name' => $this->name,
            'price' => $this->price
        ];
    }
}
