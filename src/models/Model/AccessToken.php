<?php

namespace PriseDeCourant\Model;

use JetBrains\PhpStorm\Pure;
use PriseDeCourant\Model\Repository\TokenTypeRepository;
use PriseDeCourant\Model\Repository\UserRepository;

class AccessToken extends BaseModel
{

    protected int $userId;
    protected string $token;
    protected int $tokenType;
    protected string $expirationDate;

    protected TokenType $type;

    /**
     * {@inheritdoc}
     */
    public function id(): int
    {
        return $this->userId;
    }

    /**
     * {@inheritdoc}
     */
    public function label(): string
    {
        return $this->token;
    }

    /**
     * Returns a token.
     *
     * @return string
     *   Token.
     */
    #[Pure] public function getToken(): string
    {
        return $this->label();
    }

    /**
     * Get the expiration date as an object.
     *
     * @return \DateTime|bool
     *   DateTime object, FALSE on failure.
     */
    public function getExpirationDate(): \DateTime|bool
    {
        return \DateTime::createFromFormat('Y-m-d H:i:s', $this->expirationDate);
    }

    /**
     * Get the current token type.
     *
     * @return TokenType
     *   Token type.
     */
    public function getTokenType(): TokenType
    {
        return $this->type;
    }

    /**
     * Get a user by token's UID field.
     *
     * @return User|null
     *   User if found, else null.
     */
    public function getUser(): User|null
    {
        $userRepository = new UserRepository();
        return $userRepository->findById($this->id());
    }

    /**
     * {@inheritdoc}
     */
    public static function map(array $values): BaseModel
    {
        $repository = new TokenTypeRepository();
        /** @var AccessToken $token */
        $token = parent::map($values);

        $token->type = $repository->findById($token->tokenType);

        return $token;
    }
}
