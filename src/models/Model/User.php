<?php

namespace PriseDeCourant\Model;

use JetBrains\PhpStorm\Pure;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use PriseDeCourant\Model\Repository\RoleRepository;

class User extends BaseModel
{

    protected int $userId;
    protected string $roleId;
    protected string $email;
    protected string $password;
    protected string $firstName;
    protected string $lastName;
    protected string $phoneNumber;
    protected bool $verified;

    protected Role $role;

    /**
     * {@inheritdoc}
     */
    public function id(): int
    {
        return $this->userId;
    }

    /**
     * {@inheritdoc}
     */
    public function label(): string
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * Return e-mail.
     *
     * @return string
     *   E-mail.
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Return phone number.
     *
     * @return string
     *   Phone number.
     */
    public function getNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * Return phone number in an international format.
     *
     * @return string
     *   Phone number.
     */
    public function getInternationalNumber(): string
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        $number = $phoneUtil->parse($this->phoneNumber, 'RS');
        return $phoneUtil->format($number, PhoneNumberFormat::INTERNATIONAL);
    }

    /**
     * Return role label.
     *
     * @return string
     *   Role label.
     */
    #[Pure] public function getRole(): string
    {
        return $this->role->label();
    }

    /**
     * Check if the user is verified.
     *
     * @return bool
     *   Verification status.
     */
    public function isVerified(): bool
    {
        return $this->verified;
    }

    /**
     * Try to verify password hash.
     *
     * @param string $password
     *   Cleartext password.
     *
     * @return bool
     *   Returns TRUE if the password is correct.
     */
    public function isPasswordCorrect(string $password): bool
    {
        return password_verify($password, $this->password);
    }

    /**
     * {@inheritdoc}
     */
    public static function map(array $values): BaseModel
    {
        $repository = new RoleRepository();
        /** @var User $user */
        $user = parent::map($values);

        $user->role = $repository->findById($user->roleId);

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        return [
            'email' => $this->email,
            'name' => $this->firstName . ' ' . $this->lastName,
            'phone' => $this->phoneNumber,
            'role' => $this->getRole(),
        ];
    }
}
