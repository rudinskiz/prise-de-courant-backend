<?php

namespace PriseDeCourant\Model;

use JetBrains\PhpStorm\Pure;

abstract class BaseModel
{

    /**
     * Get the entity ID.
     *
     * @return mixed
     *   Entity ID.
     */
    public function id(): mixed
    {
        // Not implemented.
        return -1;
    }

    /**
     * Get the entity label.
     *
     * @return string
     *   Entity label.
     */
    public function label(): string
    {
        // Not implemented.
        return '';
    }

    /**
     * Maps the values to the class.
     *
     * @param array $values
     *   Values.
     *
     * @return BaseModel
     *   Instance.
     */
    #[Pure] public static function map(array $values): BaseModel
    {
        $class = get_called_class();
        $instance = new $class();
        foreach ($values as $field => $value) {
            if (property_exists($instance, $field)) {
                $instance->{$field} = $value;
            }
        }

        return $instance;
    }

    /**
     * Return the class as an array.
     *
     * @return array
     *   Array with fields.
     */
    public function toArray(): array
    {
        return [];
    }
}
