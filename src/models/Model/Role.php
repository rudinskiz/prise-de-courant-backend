<?php

namespace PriseDeCourant\Model;

class Role extends BaseModel
{

    protected int $roleId;
    protected string $roleName;

    /**
     * {@inheritdoc}
     */
    public function id(): int
    {
        return $this->roleId;
    }

    /**
     * {@inheritdoc}
     */
    public function label(): string
    {
        return $this->roleName;
    }
}
