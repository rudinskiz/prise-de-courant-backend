<?php

namespace PriseDeCourant\Model\Repository;

use PriseDeCourant\Model\AccessToken;

class AccessTokenRepository extends BaseModelRepository
{

    protected string $tableName = 'AccessToken';
    protected string $primaryKey = 'token';
    protected string $modelClass = AccessToken::class;
}
