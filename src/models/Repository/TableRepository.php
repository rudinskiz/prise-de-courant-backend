<?php

namespace PriseDeCourant\Model\Repository;

use PriseDeCourant\Model\Table;

class TableRepository extends BaseModelRepository
{

    protected string $tableName = '`Table`';
    protected string $primaryKey = 'tableId';
    protected string $modelClass = Table::class;
}
