<?php

namespace PriseDeCourant\Model\Repository;

use PriseDeCourant\Model\TokenType;

class TokenTypeRepository extends BaseModelRepository
{

    protected string $tableName = 'TokenType';
    protected string $primaryKey = 'tokenTypeID';
    protected string $modelClass = TokenType::class;

    /**
     * Get a token type by name.
     *
     * @param string $typeName
     *   Type name.
     *
     * @return mixed
     *   Token type model.
     */
    public function getByName(string $typeName): mixed
    {
        $types = $this->findWhere(['tokenType' => $typeName]);
        if (empty($types)) {
            return NULL;
        }

        return reset($types);
    }
}
