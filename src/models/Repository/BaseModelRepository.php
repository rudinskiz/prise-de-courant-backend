<?php

namespace PriseDeCourant\Model\Repository;

use JetBrains\PhpStorm\Pure;
use PriseDeCourant\Model\BaseModel;
use PriseDeCourant\Services\ServiceContainer;

/**
 * Base database model.
 *
 * @package PriseDeCourant\Model
 */
abstract class BaseModelRepository
{

    /**
     * Primary key column name.
     *
     * @var string
     */
    protected string $primaryKey = 'id';

    /**
     * Model class.
     *
     * @var string
     */
    protected string $modelClass = BaseModel::class;

    /**
     * Name of the table in the DB.
     * Defaults to class name.
     *
     * @var string
     */
    protected string $tableName;

    /**
     * List of all fields.
     *
     * @var array
     */
    protected array $fields = [];

    /**
     * Get a result via primary key.
     *
     * @param mixed $id
     *   Primary key value.
     * @param array $fields
     *   List of fields.
     *
     * @return mixed
     *   Selected value.
     */
    public function findById(mixed $id = 1, array $fields = []): mixed
    {
        [$fields, $tableName] = $this->getBaseInfo($fields);

        $result = ServiceContainer::get('database')->query("SELECT $fields FROM $tableName WHERE $this->primaryKey = :id", [
            ':id' => $id
        ]);

        if (empty($result)) {
            return NULL;
        }

        return call_user_func_array([$this->modelClass, 'map'], [end($result)]);
    }

    /**
     * Return number of records in the database.
     *
     * @param array $condition
     *   Conditions.
     * @param string $conditionString
     *   Condition string.
     *
     * @return int
     *   Number of records.
     */
    public function count(array $condition = [], string $conditionString = 'AND'): int
    {
        $tableName = $this->tableName;
        $query = "SELECT COUNT(*) FROM $tableName";
        $parameters = [];
        if (!empty($condition)) {
            [$conditionString, $parameters] = $this->createCondition($condition, $conditionString);
            $query .= " WHERE " . $conditionString;
        }

        $result = ServiceContainer::get('database')->query($query, $parameters);
        $result = end($result);

        return (int) reset($result);
    }

    /**
     * Find all.
     *
     * @param array $fields
     *   List of fields.
     *
     * @return array
     *   Selected models.
     */
    public function findAll(array $fields = [], int $limit = -1, int $offset = -1): array
    {
        [$fields, $tableName] = $this->getBaseInfo($fields);
        $query = "SELECT $fields FROM $tableName";
        if ($limit !== -1 && $offset !== -1) {
            $query .= " LIMIT $offset, $limit";
        }
        $results = ServiceContainer::get('database')->query($query);
        return array_map(function ($result) {
            return call_user_func_array([$this->modelClass, 'map'], [$result]);
        }, $results);
    }

    /**
     * Find by condition.
     *
     * @param array $condition
     *   Condition.
     * @param array $fields
     *   Fields.
     * @param string $conjunction
     *   Conjunction.
     *
     * @return array
     *   Results.
     */
    public function findWhere(array $condition, array $fields = [], string $conjunction = 'AND'): array
    {
        [$fields, $tableName] = $this->getBaseInfo($fields);
        [$condition, $parameters] = $this->createCondition($condition, $conjunction);

        // Construct the query string.
        $query = "SELECT $fields FROM $tableName WHERE $condition";
        $results = ServiceContainer::get('database')->query($query, $parameters);
        $results = array_map(function ($result) {
            return call_user_func_array([$this->modelClass, 'map'], [$result]);
        }, $results);

        return $results;
    }

    /**
     * Delete from the database.
     *
     * @param array $condition
     *   Condition, or and empty array if you want to delete everything.
     * @param string $conjunction
     *   Conjunction string.
     *
     * @return boolean
     *   Query result.
     */
    public function delete(array $condition = [], string $conjunction = 'AND'): bool
    {
        $tableName = $this->getBaseInfo([])[1];
        if (!empty($condition)) {
            [$condition, $parameters] = $this->createCondition($condition, $conjunction);
            // Construct the query string.
            $query = "DELETE FROM $tableName WHERE $condition";
        } else {
            $query = "DELETE FROM $tableName";
        }

        return ServiceContainer::get('database')->execute($query, $parameters ?? []);
    }

    /**
     * Update a table entry via condition.
     *
     * @param array $condition
     *   Condition.
     * @param array $fields
     *   Fields to be updated.
     * @param string $conjunction
     *   Conjunction.
     *
     * @return bool
     *   Result of the query.
     */
    public function update(array $condition, array $fields, $conjunction = 'AND'): bool
    {
        if (empty($fields) || empty($condition)) {
            return FALSE;
        }

        $tableName = $this->getBaseInfo([])[1];
        [$condition, $parameters] = $this->createCondition($condition, $conjunction);

        // Create an update query.
        $query = "UPDATE $tableName SET ";
        foreach ($fields as $field => $value) {
            $parameters[":$field"] = $value;
            $query .= "$field=:$field,";
        }

        $query = trim($query, ',');
        $query .= " WHERE $condition";

        return ServiceContainer::get('database')->execute($query, $parameters);
    }

    /**
     * Insert something into the database.
     *
     * @param array $values
     *   Array of values.
     *
     * @return mixed
     *   Inserted model.
     */
    public function insert(array $values): mixed
    {
        $tableName = $this->getTableName();
        $fieldNames = implode(',', array_keys($values));

        $parameters = $this->createCondition($values)[1];
        $parameterNames = implode(',', array_keys($parameters));

        $query = "INSERT INTO " . $tableName . " ($fieldNames) VALUES ($parameterNames)";

        if (ServiceContainer::get('database')->execute($query, $parameters)) {
            return $this->findWhere($values)[0];
        }

        return NULL;
    }

    /**
     * Create/update.
     *
     * @param array $values
     *   Values.
     *
     * @return mixed
     *   The resulting entity.
     */
    public function save(array $values): mixed
    {
        // Check if there already existing something.
        if (isset($values[$this->primaryKey])) {
            $entity = $this->findById($values[$this->primaryKey]);
            if ($entity) {
                // Update it.
                $this->update([
                    $this->primaryKey => $values[$this->primaryKey],
                ], $values);

                return $this->findById($values[$this->primaryKey]);
            }
        } else {
            $entities = $this->findWhere($values);
            if (!empty($entities)) {
                $entity = reset($entities);

                // Update it.
                $this->update([
                    $this->primaryKey => $entity[$this->primaryKey],
                ], $values);

                return $this->findWhere($values)[0];
            }
        }
        // Try to create it.
        return $this->insert($values);
    }

    /**
     * Create a condition string and parameters.
     *
     * @param array $condition
     *   Conditions.
     * @param string $conjunction
     *   Conjunction operator.
     *
     * @return array
     *   Condition information.
     */
    private function createCondition(array $condition, string $conjunction = 'AND'): array
    {
        // Construct a condition string.
        $conditionString = '';
        $parameters = [];
        foreach ($condition as $field => $fieldInfo) {
            $operator = $fieldInfo['operator'] ?? '=';

            $conditionString .= " $conjunction $field $operator :$field";
            $parameters[":$field"] = $fieldInfo['value'] ?? $fieldInfo;
        }
        $conditionString = trim($conditionString, " $conjunction ");

        return [$conditionString, $parameters];
    }

    /**
     * Gets the table name.
     *
     * @return string
     */
    private function getTableName(): string
    {
        if (empty($this->tableName)) {
            $class = explode('\\', get_class($this));
            return end($class);
        }
        return $this->tableName;
    }

    /**
     * Fields that will be selected.
     *
     * @param array $fields
     *   List of fields.
     *
     * @return string
     *   List of fields that need to be fetched.
     */
    #[Pure] private function getFields(array $fields): string
    {
        if (empty($fields)) {
            $fields = implode(',', $this->fields);
            if (empty($fields)) {
                $fields = '*';
            }
        } else {
            $fields = implode(',', $fields);
        }
        return $fields;
    }

    /**
     * Get the base info for every query.
     *
     * @param array $fields
     *   Fields.
     *
     * @return array
     *   Base info (array, table name).
     */
    private function getBaseInfo(array $fields): array
    {
        // Get base info.
        $fields = $this->getFields($fields);
        $tableName = $this->getTableName();
        return [$fields, $tableName];
    }
}
