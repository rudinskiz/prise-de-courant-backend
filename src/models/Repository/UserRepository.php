<?php

namespace PriseDeCourant\Model\Repository;

use PriseDeCourant\Model\User;

class UserRepository extends BaseModelRepository
{

    protected string $tableName = 'User';
    protected string $primaryKey = 'userId';
    protected string $modelClass = User::class;

    /**
     * Return a user by e-mail.
     *
     * @param string $email
     *   E-mail.
     *
     * @return mixed
     *   User model.
     */
    public function getByEmail(string $email): mixed
    {
        $users = $this->findWhere(['email' => $email]);
        if (empty($users)) {
            return NULL;
        }
        return reset($users);
    }
}
