<?php

namespace PriseDeCourant\Model\Repository;

class RestaurantMenuItemRepository extends BaseModelRepository
{

    protected string $tableName = 'RestaurantMenu';
    protected string $primaryKey = 'itemId';
}
