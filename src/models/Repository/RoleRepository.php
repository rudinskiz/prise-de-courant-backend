<?php

namespace PriseDeCourant\Model\Repository;

use PriseDeCourant\Model\Role;

class RoleRepository extends BaseModelRepository
{

    protected string $tableName = 'Role';
    protected string $primaryKey = 'roleId';
    protected string $modelClass = Role::class;

    /**
     * Get role by name.
     *
     * @param string $roleName
     *   Role name.
     *
     * @return mixed
     *   Return role if it exists, else null.
     */
    public function getByName(string $roleName): mixed
    {
        $roles = $this->findWhere(['roleName' => $roleName]);
        if (empty($roles)) {
            return NULL;
        }
        return reset($roles);
    }
}
