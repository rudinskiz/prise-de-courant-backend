<?php

namespace PriseDeCourant\Model\Repository;

use PriseDeCourant\Model\Reservation;

class ReservationRepository extends BaseModelRepository
{

    protected string $tableName = 'Reservation';
    protected string $primaryKey = 'reservationCode';
    protected string $modelClass = Reservation::class;
}
