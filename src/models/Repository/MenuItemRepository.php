<?php

namespace PriseDeCourant\Model\Repository;

use PriseDeCourant\Model\MenuItem;

class MenuItemRepository extends BaseModelRepository
{
	protected string $tableName = 'MenuItem';
	protected string $primaryKey = 'menuItemId';
	protected string $modelClass = MenuItem::class;
}
