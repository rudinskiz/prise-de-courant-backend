<?php

namespace PriseDeCourant\Validators;

use Countable;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;

/**
 * Handles field validation.
 *
 * @package PriseDeCourant\Validators
 */
class FieldValidator
{

    public const FIELD_PASSWORD = 'password';
    public const FIELD_EMAIL = 'email';
    public const FIELD_PHONE_NUMBER = 'phone_number';
    public const FIELD_NOT_EMPTY = 'not_empty';
    public const FIELD_NUMBER_RANGE = 'number_range';

    public const FIELD_RANGE_MIN = 'range_min';
    public const FIELD_RANGE_MAX = 'range_max';

    /**
     * Validate all fields grouped by their type.
     *
     * @param array $fields
     *   Array of types containing all the fields.
     *
     * @return bool
     *   Validation result.
     */
    public static function validateAll(array $fields): bool
    {
        foreach ($fields as $type => $field) {
            foreach ($field as $item) {
                if (is_array($item)) {
                    if (!self::validateField($item[0], $type, $item[1])) {
                        return false;
                    }
                } else {
                    if (!self::validateField($item, $type)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Validate a field.
     *
     * @param mixed $field
     *   Field value.
     *
     * @param string $fieldType
     *   Type of field to validate.
     *
     * @param array $extraData
     *   Additional data needed for validation.
     *
     * @return bool
     *   Validation result.
     */
    public static function validateField(mixed $field, string $fieldType, array $extraData = []): bool
    {
        switch ($fieldType) {
            case self::FIELD_NOT_EMPTY:
                if ($field instanceof Countable) {
                    return count($field) > 0;
                }
                if (is_string($field)) {
                    return mb_strlen($field) > 0;
                }
                return !empty($field);

            case self::FIELD_EMAIL:
                return filter_var($field, FILTER_VALIDATE_EMAIL);

            case self::FIELD_PASSWORD:
                $length = mb_strlen($field);
                return  is_string($field) && (8 <= $length) && ($length <= 24);

            case self::FIELD_PHONE_NUMBER:
                $phoneUtil = PhoneNumberUtil::getInstance();
                try {
                    $number = $phoneUtil->parse($field, "RS");
                    return $phoneUtil->isValidNumber($number);
                } catch (NumberParseException) {
                    return false;
                }

            case self::FIELD_NUMBER_RANGE:
                $min = $extraData[self::FIELD_RANGE_MIN] ?? NULL;
                $max = $extraData[self::FIELD_RANGE_MAX] ?? NULL;

                if ($min !== NULL && $field < $min) {
                    return false;
                }
                if ($max !== NULL && $field > $max) {
                    return false;
                }

                return true;
        }
        return false;
    }
}
