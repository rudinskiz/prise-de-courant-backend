<?php

namespace PriseDeCourant\Validators;

use JetBrains\PhpStorm\Pure;
use PriseDeCourant\Model\User;

/**
 * Handles user validation.
 *
 * @package PriseDeCourant\Validators
 */
class UserValidator
{

    /**
     * Check if a user has a specified role.
     *
     * @param ?User $user
     *   User.
     * @param string $roleName
     *   Role.
     *
     * @return bool
     *   True if the user has the specified role.
     */
    #[Pure] public static function isRole(?User $user, string $roleName): bool
    {
        if ($user === NULL) {
            return false;
        }
        return $user->isVerified() && $user->getRole() === $roleName;
    }
}
