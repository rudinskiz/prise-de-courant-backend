<?php

namespace PriseDeCourant\Utils;

/**
 * Class containing all necessary HTTP status codes.
 *
 * @package PriseDeCourant\Utils
 */
abstract class ResponseCode
{
    public const OK = 200;
    public const NOT_FOUND = 404;
    public const BAD_REQUEST = 400;
    public const UNAUTHORIZED = 401;
    public const SERVER_ERROR = 500;
}
