<?php

namespace PriseDeCourant\Utils;

use PriseDeCourant\Core\Exception\ResponseException;
use PriseDeCourant\Services\ServiceContainer;

/**
 * A wrapper class around the response service.
 *
 * @package PriseDeCourant\Utils
 */
abstract class Response
{

    /**
     * Prints a response exception properly to the user.
     *
     * @param ResponseException $exception
     *   Exception instance.
     * @param int $responseCode
     *   HTTP status code.
     */
    public static function printError(ResponseException $exception, int $responseCode)
    {
        ServiceContainer::get('response')->write([
            'status' => $responseCode,
            'message' => (string) $exception
        ], statusCode: $responseCode);
    }

    /**
     * Prints a message to the users.
     *
     * @param string $message
     *   A message.
     */
    public static function printMessage(string $message)
    {
        ServiceContainer::get('response')->write([
            'status' => ResponseCode::OK,
            'message' => $message
        ]);
    }

    /**
     * Print an array to the users.
     *
     * @param array $values
     *   Array.
     */
    public static function printArray(array $values)
    {
        ServiceContainer::get('response')->write([
            'status' => ResponseCode::OK,
            'data' => $values,
        ]);
    }
}
