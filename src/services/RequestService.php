<?php

namespace PriseDeCourant\Services;

use PriseDeCourant\Core\Exception\ParameterNotFoundException;

/**
 * A service that handles request data.
 *
 * @package PriseDeCourant\Services
 */
class RequestService
{

    /**
     * Return a GET parameter.
     *
     * @param string $name
     *   Parameter name.
     * @param mixed $default
     *   Default return value.
     *
     * @return mixed
     *   Parameter value.
     */
    public function getQuery(string $name, $default = NULL): mixed
    {
        return $_GET[$name] ?? $default;
    }

    /**
     * Return a POST parameter.
     *
     * @param string $name
     *   Parameter name.
     * @param mixed $default
     *   Default return value.
     *
     * @return mixed
     *   Parameter value.
     */
    public function getParameter(string $name, $default = NULL): mixed
    {
        $_JSON = (array) json_decode(file_get_contents('php://input'));
        if (array_key_exists($name, $_POST)) {
            return $_POST[$name];
        } elseif (array_key_exists($name, $_JSON)) {
            return $_JSON[$name];
        }

        return $default;
    }

    /**
     * Return a FILE parameter.
     *
     * @param string $name
     *   Parameter name.
     * @param mixed $default
     *   Default return value.
     *
     * @return mixed
     *   Parameter value.
     */
    public function getFile(string $name, $default = NULL): mixed
    {
        return $_FILES[$name] ?? $default;
    }

    /**
     * Return a GET parameter and if it's empty throws an exception.
     *
     * @param string $name
     *   Parameter name.
     * @param mixed $default
     *   Default return value.
     *
     * @return mixed
     *   Parameter value.
     *
     * @throws ParameterNotFoundException
     *   Parameter not found or is empty.
     */
    public function getQueryWithException(string $name, $default = NULL): mixed
    {
        $value = $this->getQuery($name, $default);
        if ($value === $default) {
            throw new ParameterNotFoundException($name);
        }
        return $value;
    }

    /**
     * Return a POST parameter and if it's empty throws an exception.
     *
     * @param string $name
     *   Parameter name.
     * @param mixed $default
     *   Default return value.
     *
     * @return mixed
     *   Parameter value.
     *
     * @throws ParameterNotFoundException
     *   Parameter not found or is empty.
     */
    public function getParameterWithException(string $name, $default = NULL): mixed
    {
        $value = $this->getParameter($name, $default);
        if ($value === $default) {
            throw new ParameterNotFoundException($name);
        }
        return $value;
    }
}
