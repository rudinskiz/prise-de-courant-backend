<?php

namespace PriseDeCourant\Services;

use PriseDeCourant\Core\Exception\AccessException;
use PriseDeCourant\Core\Exception\ParameterNotFoundException;
use PriseDeCourant\Model\AccessToken;
use PriseDeCourant\Model\Repository\AccessTokenRepository;
use PriseDeCourant\Model\Repository\TokenTypeRepository;
use PriseDeCourant\Model\TokenType;
use PriseDeCourant\Model\User;
use RandomLib\Factory;
use RandomLib\Generator;

class TokenService
{

    private TokenTypeRepository $tokenTypeRepository;
    private AccessTokenRepository $accessTokenRepository;
    public Generator $generator;
    public const characterList = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    public function __construct()
    {
        $this->generator = (new Factory())->getMediumStrengthGenerator();
        $this->accessTokenRepository = new AccessTokenRepository();
        $this->tokenTypeRepository = new TokenTypeRepository();
    }

    /**
     * Generate a new token.
     *
     * @param User $user
     *   User for whom to generate the token.
     * @param string $tokenType
     *   Token type.
     *
     * @return AccessToken
     *   Generated token.
     *
     * @throws ParameterNotFoundException
     *   If the token type is invalid.
     */
    public function generate(User $user, string $tokenType): AccessToken
    {
        // Get the token type.
        /** @var TokenType $tokenTypeModel */
        $tokenTypeModel = $this->tokenTypeRepository->getByName($tokenType);
        if ($tokenTypeModel === NULL) {
            throw new ParameterNotFoundException($tokenType);
        }
        // Clear out old tokens.
        $this->accessTokenRepository->delete(['userId' => $user->id(), 'tokenType' => $tokenTypeModel->id()]);
        $expirationDate = (new \DateTime())->modify($_ENV['TOKEN_VALIDITY'])->format('Y-m-d H:i:s');
        // Create a  token.
        /** @var AccessToken $token */
        return $this->accessTokenRepository->save([
            'userId' => $user->id(),
            'token' => $this->generator->generateString($_ENV['TOKEN_SIZE'], self::characterList),
            'tokenType' => $tokenTypeModel->id(),
            'expirationDate' => $expirationDate,
        ]);
    }

    /**
     * Get the token.
     *
     * @param User|null $user
     *   User for whom to generate the token.
     * @param string $tokenType
     *   Token type.
     * @param string $tokenValue
     *   Token value.
     *
     * @return AccessToken
     *   Generated token.
     *
     * @throws AccessException
     *   If the token is invalid.
     * @throws ParameterNotFoundException
     *   If the token type is invalid.
     */
    public function getToken(string $tokenType, string $tokenValue, User $user = NULL): AccessToken
    {
        // Get the token type.
        /** @var TokenType $tokenTypeModel */
        $tokenTypeModel = $this->tokenTypeRepository->getByName($tokenType);
        if ($tokenTypeModel === NULL) {
            throw new ParameterNotFoundException($tokenType);
        }
        // Search for token.
        $fields = [
            'token' => $tokenValue,
            'tokenType' => $tokenTypeModel->id(),
        ];
        if ($user !== NULL) {
            $fields['userId'] = $user->id();
        }
        // Get the token.
        $tokens = $this->accessTokenRepository->findWhere($fields, [
            'userId',
            'expirationDate',
            'tokenType',
            'token'
        ]);
        if (count($tokens) < 1) {
            throw new ParameterNotFoundException('token');
        }
        /** @var AccessToken $token */
        $token = reset($tokens);
        // Check if token is valid.
        if ($token->getExpirationDate() < new \DateTime()) {
            throw new AccessException('Token expired.');
        }

        return $token;
    }
}
