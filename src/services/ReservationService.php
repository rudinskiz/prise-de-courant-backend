<?php

namespace PriseDeCourant\Services;

use PriseDeCourant\Model\Repository\ReservationRepository;
use PriseDeCourant\Services\DatabaseService;
use PriseDeCourant\Model\Reservation;

/**
 * A service that handles reservations.
 */
class ReservationService
{

    protected ReservationRepository $reservationRepository;
    protected DatabaseService $databaseService;

    public function __construct()
    {
        $this->reservationRepository = new ReservationRepository();
        $this->databaseService = new DatabaseService();
    }

    private function isOnTheSameDay($timestamp1, $timestamp2 = "")
    {
        if ($timestamp2 == "") {
            $timestamp2 = time();
        }
        return date("z-Y", $timestamp1) === date("z-Y", $timestamp2);
    }

    public function addItemToReservation(Reservation $reservation, $itemId)
    {
        $this->databaseService->execute("INSERT INTO ReservationItem (reservationId, 	menuItemId) VALUES (:rid, :mid)", [':rid' => $reservation->id(), ':mid' => $itemId]);
    }

    public function removeItemFromReservation(Reservation $reservation, $itemId)
    {
        $this->databaseService->execute("DELETE FROM ReservationItem WHERE reservationId = :rid AND menuItemId = :mid", [':rid' => $reservation->id(), ':mid' => $itemId]);
    }

    public function setOrderStatus(Reservation $reservation, $status)
    {
        $this->databaseService->execute("UPDATE Reservation SET state = :status WHERE reservationId = :rid", [':rid' => $reservation->id(), ':status' => $status]);
    
    }

    public function getReservationItems(Reservation $reservation) {
      $items = $this->databaseService->query('SELECT MenuItem.name, ReservationItem.menuItemId AS id, SUM(price) AS price FROM `ReservationItem` JOIN MenuItem ON ReservationItem.menuItemId = MenuItem.menuItemId WHERE ReservationItem.reservationId = :rid GROUP BY MenuItem.name', [':rid' => $reservation->id()]);

      return $items;
    }

    public function checkAvailability($tableId, $reservationStart, $reservationEnd)
    {
        /** @var Reservation[] $reservations */
        $reservations = $this->reservationRepository->findWhere([
            'tableId' => $tableId,
        ]);

        $reservations = array_filter($reservations, function ($reservation) {
            return !$reservation->isCancelled();
        });

        $reservations = array_filter($reservations, function ($reservation) use ($reservationStart, $reservationEnd) {
            $range = $reservation->getRange();
            /** @var \DateTime $startDate */
            $startDate = $range['start'];
            /** @var \DateTime $endDate */
            $endDate = $range['end'];
            // Check if the day is same.
            $startDiff = $this->isOnTheSameDay($startDate->getTimestamp(), $reservationStart->getTimestamp());
            $endDiff = $this->isOnTheSameDay($endDate->getTimestamp(), $reservationEnd->getTimestamp());

            if ($startDiff || $endDiff) {
                return TRUE;
            }

            return FALSE;
        });

        $reservations = array_filter($reservations, function ($reservation) use ($reservationStart, $reservationEnd) {
            $range = $reservation->getRange();
            /** @var \DateTime $startDate */
            $startDate = $range['start'];
            /** @var \DateTime $endDate */
            $endDate = $range['end'];

            // Check if we begin before the end date.
            if ($reservationStart >= $endDate) {
                return TRUE;
            }
            // Check if we end after a start date.
            if ($reservationEnd >= $startDate) {
                return TRUE;
            }

            return FALSE;
        });

        return empty($reservations);
    }
}
