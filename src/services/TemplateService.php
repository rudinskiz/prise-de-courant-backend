<?php

namespace PriseDeCourant\Services;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

/**
 * Class that deals with Twig templates.
 *
 * @package PriseDeCourant\Services
 */
class TemplateService
{

    /**
     * Twig template loader.
     *
     * @var FilesystemLoader
     */
    private FilesystemLoader $loader;

    /**
     * Twig renderer.
     *
     * @var Environment
     */
    private Environment $renderer;

    /**
     * TemplateService constructor.
     */
    public function __construct()
    {
        $this->loader = new FilesystemLoader(WEB_ROOT . '/templates');
        $this->renderer = new Environment($this->loader, [
            'cache' => WEB_ROOT . '/../cache'
        ]);
    }

    /**
     * Renders a Twig template.
     *
     * @param string $template
     *   Template name.
     * @param array $params
     *   Template parameters.
     * @return string
     *   Rendered HTML.
     *
     * @throws LoaderError
     *   Error with loading the template.
     * @throws RuntimeError
     *   Runtime error.
     * @throws SyntaxError
     *   Error with Twig syntax.
     */
    public function render(string $template, $params = []): string
    {
        return $this->renderer->render($template, $params);
    }
}
