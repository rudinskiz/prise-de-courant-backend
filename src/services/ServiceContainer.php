<?php

namespace PriseDeCourant\Services;

/**
 * Class that deals with necessary services and gets them on demand.
 *
 * @package PriseDeCourant\Services
 */
class ServiceContainer
{

    /**
     * Array of already instantiated services.
     * @var array
     */
    protected static array $instances = [];

    /**
     * Map of all the service classes.
     * @var string[]
     */
    protected static array $serviceMap = [
        'response' => ResponseService::class,
        'database' => DatabaseService::class,
        'template' => TemplateService::class,
        'request' => RequestService::class,
        'mail' => MailService::class,
        'token' => TokenService::class,
        'reservation' => ReservationService::class,
        'sms' => SmsService::class,
    ];

    /**
     * Get the specified service by name.
     *
     * @param $service
     *   Name of the service in the map.
     *
     * @return mixed
     *   Service instance.
     */
    public static function get($service): mixed
    {
        if (!isset(self::$instances[$service])) {
            self::$instances[$service] = new self::$serviceMap[$service];
        }

        return self::$instances[$service];
    }
}
