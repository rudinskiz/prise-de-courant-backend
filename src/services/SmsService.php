<?php

namespace PriseDeCourant\Services;

use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

/**
 * A service that handles sending of SMS messages.
 *
 * @package PriseDeCourant\Services
 */
class SmsService
{

    /**
     * Twilio client instance.
     *
     * @var Client
     */
    protected ?Client $client = NULL;

    /**
     * SmsService constructor.
     */
    public function __construct()
    {
        if ($_ENV['TWILIO_SID'] && $_ENV['TWILIO_TOKEN'] && $_ENV['TWILIO_NUMBER']) {
            $this->client = new Client($_ENV['TWILIO_SID'], $_ENV['TWILIO_TOKEN']);
        }
    }

    /**
     * Send an SMS message.
     *
     * @param string $to
     *   Phone number to send the message to.
     * @param string $message
     *   Message to send.
     *
     * @return bool
     *   TRUE if the message was sent successfully, FALSE otherwise.
     */
    public function send(string $to, string $message): bool
    {
        if ($this->client) {
            try {
                $this->client->messages->create(
                    $to,
                    [
                        'from' => $_ENV['TWILIO_NUMBER'],
                        'body' => $message,
                    ]
                );
            } catch (TwilioException $twilioException) {
                return FALSE;
            }
        }

        return FALSE;
    }
}
