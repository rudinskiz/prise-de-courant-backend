<?php

namespace PriseDeCourant\Services;

use PriseDeCourant\Utils\ResponseCode;

/**
 * A service that handles responses.
 *
 * @package PriseDeCourant\Services
 */
class ResponseService
{

    /**
     * Prints JSON-encoded contents to the response.
     *
     * @param $contents
     *   Data.
     * @param bool $encode
     *   If the data isn't encoded it'll encode it first.
     * @param int $statusCode
     *   HTTP status code.
     */
    public function write($contents, $encode = TRUE, $statusCode = ResponseCode::OK)
    {
        http_response_code($statusCode);

        if ($encode) {
            print json_encode($contents);
        } else {
            print $contents;
        }
    }
}
