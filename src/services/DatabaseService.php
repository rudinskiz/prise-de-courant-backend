<?php

namespace PriseDeCourant\Services;

use PDO;

/**
 * Class that handles DB transactions.
 *
 * @package PriseDeCourant\Services
 */
class DatabaseService
{

    /**
     * Connect to the database.
     *
     * @return PDO
     *   Connection.
     */
    private function connect(): PDO
    {
        $host = $_ENV['DB_HOST'];
        $database = $_ENV['DB_DATABASE'];

        $pdo = new PDO("mysql:host=$host;dbname=$database", $_ENV['DB_USER'], $_ENV['DB_PASSWORD']);
        // Set the error mode to exception.
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $pdo;
    }

    /**
     * Disconnects from the database.
     *
     * @param PDO $pdo
     *   PDO connection.
     */
    private function disconnect(PDO &$pdo)
    {
        $pdo = NULL;
    }

    /**
     * Execute a query and return the results.
     *
     * @param string $query
     *   Query string.
     * @param array $placeholders
     *   Placeholder.
     *
     * @return array
     *   Results.
     */
    public function query(string $query, array $placeholders = []): array
    {
        // Connect to the DB.
        $pdo = $this->connect();
        $pdoStatement = $pdo->prepare($query);

        $placeholderKeys = array_keys($placeholders);
        for ($i = 0; $i < count($placeholderKeys); $i++) {
            $pdoStatement->bindParam($placeholderKeys[$i], $placeholders[$placeholderKeys[$i]]);
        }

        // Execute the statement.
        $pdoStatement->execute();
        $result = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);

        // Disconnect from the DB.
        $this->disconnect($pdo);

        return $result;
    }

    /**
     * Execute a query without returning anything.
     *
     * @param string $query
     *   Query string.
     * @param array $placeholders
     *   Placeholder.
     *
     * @return bool
     *   Result of the query.
     */
    public function execute(string $query, array $placeholders = []): bool
    {
        // Connect to the DB.
        $pdo = $this->connect();
        $pdoStatement = $pdo->prepare($query);

        $placeholderKeys = array_keys($placeholders);
        for ($i = 0; $i < count($placeholderKeys); $i++) {
            $pdoStatement->bindParam($placeholderKeys[$i], $placeholders[$placeholderKeys[$i]]);
        }

        // Execute the statement.
        $result = $pdoStatement->execute();

        // Disconnect from the DB.
        $this->disconnect($pdo);

        return $result;
    }
}
