<?php

namespace PriseDeCourant\Services;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * A service that handles mail.
 *
 * @package PriseDeCourant\Services
 */
class MailService
{

    /**
     * A mailer instance.
     *
     * @var PHPMailer
     */
    protected PHPMailer $mailer;

    /**
     * MailService constructor.
     */
    public function __construct()
    {
        $this->mailer = new PHPMailer(TRUE);
        // Settings.
        $this->mailer->isSMTP();
        $this->mailer->Host = $_ENV['SMTP_HOST'];
        $this->mailer->SMTPAuth = TRUE;
        $this->mailer->Username = $_ENV['SMTP_USER'];
        $this->mailer->Password = $_ENV['SMTP_PASSWORD'];
        $this->mailer->Port = $_ENV['SMTP_PORT'];
        $this->mailer->setFrom($_ENV['SMTP_FROM']);
        $this->mailer->isHTML(TRUE);
    }

    /**
     * Send an e-mail.
     *
     * @param string $recipient
     *   Recipient e-mail address.
     * @param string $subject
     *   Subject.
     * @param string $body
     *   Body.
     *
     * @return bool
     *   TRUE if mail was sent.
     *
     * @throws Exception
     *   An error is thrown if the message could not be sent.
     */
    public function send(string $recipient, string $subject, string $body): bool
    {
        // Clear previous settings.
        $this->mailer->clearAllRecipients();
        // Specify information.
        $this->mailer->addAddress($recipient);
        $this->mailer->Subject = $subject;
        $this->mailer->Body = $body;
        // Send.
        return $this->mailer->send();
    }
}
